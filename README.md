# ASG - ECS Cluster

## Usage

```terraform
module "ecs_asg" {
  source = "git::https://gitlab.com/itcrowdarg-public/infrastructure/terraform-modules/aws/asg-ecs-cluster.git?ref=X.Y"

  environments                    = var.environments
  vpc                             = module.vpc
  key_pair                        = aws_key_pair.main-key
  bastion_security_group_id       = module.bastion.security_group_bastion_allow_ssh_id
  security_group_alb_allow_web_id = module.load_balancer.security_group_alb_allow_web_id
}
```