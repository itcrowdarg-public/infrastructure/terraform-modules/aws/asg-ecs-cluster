resource "aws_iam_instance_profile" "cluster" {
  name = "cluster-instance-profile-ecs-cluster"
  path = "/"
  role = aws_iam_role.cluster_instance_role.name
}

resource "aws_iam_role" "cluster_instance_role" {
  description        = "cluster-instance-role-ecs-cluster"
  assume_role_policy = file("${path.module}/policies/cluster-instance-role.json")
}

resource "aws_iam_policy_attachment" "cluster_instance_policy_attachment" {
  name       = "cluster-instance-policy-attachment-ecs-cluster"
  roles      = [aws_iam_role.cluster_instance_role.id]
  policy_arn = aws_iam_policy.cluster_instance_policy.arn
}

resource "aws_iam_policy" "cluster_instance_policy" {
  description = "cluster-instance-policy-ecs-cluster"
  policy      = data.template_file.cluster_instance_policy.rendered
}


data "template_file" "cluster_instance_policy" {
  template = file("${path.module}/policies/cluster-instance-policy.json")
}

resource "null_resource" "iam_wait" {
  depends_on = [
    aws_iam_role.cluster_instance_role,
    aws_iam_policy.cluster_instance_policy,
    aws_iam_policy_attachment.cluster_instance_policy_attachment,
    aws_iam_instance_profile.cluster
  ]

  provisioner "local-exec" {
    command = "sleep 30"
  }
}

