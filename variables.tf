variable "environments" {
  description = "List of available environments"
}

variable "vpc" {
  description = "VPC where the ASG is created"
}

variable "key_pair" {
  description = "Key pair for the ASG hosts"
}

variable "bastion_security_group_id" {
  description = "Bastion's security group ID, to allow ssh from it into cluster's hosts"
}

variable "security_group_alb_allow_web_id" {
  description = "Security group ID to allow HTTP traffic from internet into ALB"
}
