output "cluster_host_security_group_id" {
  description = "Cluster Host Security Group"
  value       = aws_security_group.cluster_host_allow_bastion_and_alb.id
}
