
resource "aws_security_group" "cluster_host_allow_bastion_and_alb" {
  name        = "cluster_host_allow_bastion_and_alb"
  description = "Allow ssh inbound traffic from bastion"
  vpc_id      = var.vpc.vpc_id

  ingress {
    description     = "All ports from Bastion"
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    security_groups = [var.bastion_security_group_id]
  }

  ingress {
    description     = "All ports from ALB"
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    security_groups = [var.security_group_alb_allow_web_id]
  }

  ingress {
    description = "All ports from same ECS Cluster"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    self        = true
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}
