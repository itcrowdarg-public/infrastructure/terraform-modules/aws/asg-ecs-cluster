data "aws_ssm_parameter" "ecs_ami" {
  name = "/aws/service/ecs/optimized-ami/amazon-linux-2023/recommended/image_id"
}

data "template_file" "cluster_user_data" {
  for_each = { for i, v in var.environments : i => v }
  template = file("${path.module}/user-data/cluster.tpl")

  vars = {
    cluster_name = each.value.name
  }
}

resource "aws_launch_configuration" "clusters" {
  for_each      = { for i, v in var.environments : i => v }
  name_prefix   = "ecs-cluster-${each.value.name}"
  instance_type = each.value.ecs_cluster.instance_type
  image_id      = data.aws_ssm_parameter.ecs_ami.value
  key_name      = var.key_pair.key_name

  iam_instance_profile = aws_iam_instance_profile.cluster.name

  user_data = data.template_file.cluster_user_data[each.key].rendered

  security_groups             = [aws_security_group.cluster_host_allow_bastion_and_alb.id]
  associate_public_ip_address = false

  depends_on = [
    null_resource.iam_wait
  ]

  lifecycle {
    create_before_destroy = true
    # ignore_changes        = [image_id]
  }
}

resource "aws_autoscaling_group" "clusters" {

  for_each    = { for i, v in var.environments : i => v }
  name_prefix = "ecs-cluster-${each.value.name}-"

  vpc_zone_identifier = var.vpc.private_subnets

  launch_configuration = aws_launch_configuration.clusters[each.key].name

  desired_capacity = each.value.ecs_cluster.desired_capacity
  max_size         = each.value.ecs_cluster.desired_capacity
  min_size         = each.value.ecs_cluster.desired_capacity

  protect_from_scale_in = false

  termination_policies = ["OldestInstance"]

  tag {
    key                 = "Name"
    value               = "cluster-worker-${each.value.name}"
    propagate_at_launch = true
  }

  tag {
    key                 = "ClusterName"
    value               = each.value.name
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }

}
